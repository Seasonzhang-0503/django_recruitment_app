from django.urls import path
from django.urls.resolvers import URLPattern
from jobs import views

urlpatterns = [
    #职位列表
    path("joblist/",views.joblist,name="joblist"),

    #职位详情
    path("job/<int:job_id>/",views.detail,name="detail"),
]

